TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets
INCLUDEPATH    += ../MainWindow

TARGET          = $$qtLibraryTarget(jobplugin)
DESTDIR         = ../plugins

EXAMPLE_FILES = jobplugin.json

HEADERS += \
    jobplugins.h

SOURCES += \
    jobplugins.cpp

CONFIG += install_ok



