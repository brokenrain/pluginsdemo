#ifndef JOBPLUGINS_H
#define JOBPLUGINS_H

#include <QObject>
#include "pluginsbase.h"

class jobPlugins : public QObject, public pluginsBase
{
    Q_OBJECT
    Q_INTERFACES(pluginsBase)
    Q_PLUGIN_METADATA(IID PluginsBase_iid)
//    Q_PLUGIN_METADATA(IID PluginsBase_iid FILE "")
public:
    explicit jobPlugins(QObject *parent = nullptr);
    QString getJob() override;

signals:

public slots:
};

#endif // JOBPLUGINS_H
