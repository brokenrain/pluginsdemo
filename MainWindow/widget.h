#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include "pluginsbase.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    bool loadPlugin();

private:
    Ui::Widget *ui;

    QList<QSharedPointer<pluginsBase>> m_pluginsList;
};

#endif // WIDGET_H
