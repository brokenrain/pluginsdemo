#ifndef PLUGINSBASE_H
#define PLUGINSBASE_H
#include <QString>

class pluginsBase
{
public:
    virtual ~pluginsBase() {}

    virtual QString getJob() = 0;
};

#define PluginsBase_iid "BrokenRainK.Plugin.pluginsBase"

QT_BEGIN_NAMESPACE
Q_DECLARE_INTERFACE(pluginsBase, PluginsBase_iid)
QT_END_NAMESPACE

#endif // PLUGINSBASE_H
