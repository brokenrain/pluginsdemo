#include "widget.h"
#include "ui_widget.h"
#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QPluginLoader>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    if(!loadPlugin())
    {
        QMessageBox::warning(this, "Error", "Could not load the plugin");
    }
}

Widget::~Widget()
{
    delete ui;
}

bool Widget::loadPlugin()
{
    QDir pluginsDir(qApp->applicationDirPath());
    pluginsDir.cdUp();
    pluginsDir.cdUp();
    pluginsDir.cd("plugins");
    foreach (QString fileName, pluginsDir.entryList(QStringList() << "*.dll"))
    {
        QPluginLoader pluginLoader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = pluginLoader.instance();
        if (plugin)
        {
            pluginsBase *base = qobject_cast<pluginsBase *>(plugin);
            if (base)
            {
                qDebug() << base->getJob();
                m_pluginsList.append(QSharedPointer<pluginsBase>(base));
            }
        }
        else
        {
            qDebug() << __FUNCTION__ << pluginLoader.errorString();
        }
    }

    return true;
}
